FROM		alpine:3.6
#FROM		alpine:3.7
#FROM		alpine:3.8

# ensure www-data user exists
#RUN		set -x \
#		&& addgroup -g 33 -S docker \
#		&& adduser -u 33 -D -S -G docker docker \

RUN			apk add --no-cache --virtual .persistent-deps \
# UTILIDADES
			coreutils shadow newt ca-certificates curl tar bash git vim sudo gzip supervisor nodejs nodejs-npm ncurses \
# NGINX
			nginx \
# PHP
			php7 php7-fpm php7-opcache \
			php7-gd php7-mysqli php7-zlib php7-curl php7-ctype php7-mcrypt php7-json php7-xml php7-xmlreader php7-ldap php7-soap php7-redis php7-imagick php7-openssl php7-intl php7-dom php7-pdo php7-pdo_mysql php7-pdo_sqlite php7-sqlite3 php7-sockets \
			php7-tokenizer php7-pcntl php7-xsl php7-iconv php7-bcmath php7-gettext php7-session php7-exif php7-mbstring php7-fileinfo php7-simplexml php7-apcu php7-phar php7-xmlwriter php7-zip php7-xdebug \
# MYSQL
#			mariadb mariadb-client \
            mysql mysql-client \
# VARIOS
			yarn imagemagick \
# DOCKER
			docker py-pip \
			&& pip install --upgrade pip \
			&& pip install docker-compose \
# SETTINGS
			&& groupadd -g 1000 www \
			&& sed -i 's/^CREATE_MAIL_SPOOL=yes/CREATE_MAIL_SPOOL=no/' /etc/default/useradd \
			&& useradd -d /var/www -u 1000 -g 1000 www \
			&& chown www:www /var/www \
			&& rm -rf /var/www/* \
			# install composer
			&& echo "$(curl -sS https://composer.github.io/installer.sig) -" > composer-setup.php.sig \
                        && curl -sS https://getcomposer.org/installer | tee composer-setup.php | sha384sum -c composer-setup.php.sig \
		        && php composer-setup.php -- --install-dir=/usr/local/bin --filename=composer

#			&& adduser -s /bin/bash -D user
#			&& usermod -G docker asmuser \
#			&& echo asmuser:asmws120 | chpasswd \
#			&& echo "%docker ALL=(ALL) NOPASSWD:/usr/bin/docker" >> /etc/sudoers \
#			&& echo "docker-dev" > /etc/hostname

ADD			supervisor/*.ini		/etc/supervisor.d/
ADD			nginx/nginx.conf		/etc/nginx/nginx.conf
ADD			mysql/my.cnf			/etc/mysql/my.cnf
#ADD			docker/nginx/default.conf 	/etc/nginx/conf.d/default.conf
ADD			php/www.conf			/etc/php7/php-fpm.d/www.conf
ADD			php/php-fpm.conf		/etc/php7/php-fpm.conf
ADD			entrypoint				/usr/local/bin/entrypoint
ADD         		create_database			/usr/local/bin/create_database
ADD			post_startup			/usr/local/bin/post_startup
#ADD         start_process           /usr/local/bin/start_process
#ADD         stop_process            /usr/local/bin/stop_process
#ADD         start_database          /usr/local/bin/start_database
#ADD			stop_database			/usr/local/bin/stop_database
ADD			start_services	/usr/local/bin/start_services
ADD			stop_services	/usr/local/bin/stop_services

ENV		DB_DATA_PATH=/var/lib/mysql
RUN		mkdir /run/mysqld \
		&& chown mysql:mysql /run/mysqld \
		&& chmod 600 /etc/mysql/my.cnf \
		&& chown mysql:mysql /etc/mysql/my.cnf \
		&& chown www:www /var/tmp/nginx -R

RUN		mysql_install_db --user=mysql --datadir=${DB_DATA_PATH}

EXPOSE		80
WORKDIR		/var/www
#VOLUME		["/var/www", "/var/lib/mysql"]
#HEALTHCHECK --start-period=5s --interval=30s --timeout=10s --retries=5 CMD curl --fail http://localhost:80/ || exit 1
ENTRYPOINT	["/usr/local/bin/entrypoint"]
