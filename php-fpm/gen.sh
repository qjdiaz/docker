#!/bin/bash

versions=( 5.6-fpm 7.0-fpm 7.1-fpm )

for phpver in "${versions[@]}"
do
	phpver=$phpver ./dockerfile.sh > Dockerfile-${phpver}
done

