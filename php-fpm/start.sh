#!/bin/bash

# Habilitar debug
if [[ ! -z "${XDEBUG_CONFIG}" ]]; then
	echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini
	echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini
	echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini
fi

php-fpm
