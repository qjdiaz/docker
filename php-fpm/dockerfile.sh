#!/bin/bash

# phpver="7.0"

cat << EOF
FROM php:${phpver}

# Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

# RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

# RUN pear config-set preferred_state beta

# Install modules
RUN apt-get update && apt-get install -y vim build-essential\\
		apt-transport-https ca-certificates \\
		curl \\
        libfreetype6-dev \\
        libjpeg62-turbo-dev \\
        libmcrypt-dev \\
        libpng12-dev \\
		libxml2-dev \\
		libtidy-dev \\
		zlib1g-dev libicu-dev \\
		libxslt1-dev \\
		ssh telnet \\
		yarn \\
#		imagemagick \\
		libmagickcore-dev \\
		libmagickwand-dev \\
		freetds-bin freetds-dev freetds-common libct4 libsybdb5 tdsodbc \\

	&& ln -s /usr/lib/x86_64-linux-gnu/libsybdb.a /usr/lib/libsybdb.a \\
    && ln -s /usr/lib/x86_64-linux-gnu/libsybdb.so /usr/lib/libsybdb.so \\

	&& docker-php-ext-install iconv mcrypt soap mbstring pdo_mysql mysqli opcache tidy zip xsl bcmath \\
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \\
	&& docker-php-ext-configure intl \\
    && docker-php-ext-install gd intl \\
    && docker-php-ext-install exif \\

	## Imagick
    && pecl install imagick  \\
    && docker-php-ext-enable imagick \\
EOF

# la versión 5.6 no tiene apcu
if [ "${phpver}" != '5.6-fpm' ]; then
cat << EOF
	## APCu
    && pecl install apcu \\
    && docker-php-ext-enable apcu \\
	&& pecl install channel://pecl.php.net/apcu_bc-1.0.3 \\
EOF
fi

cat << EOF
	## xdebug
	&& pecl install xdebug \\

	## HttpClient
#	&& pecl install pecl_http \\
EOF

# solo la versión 5.6 tiene httprequest en apt-get
if [ "${phpver}" == '5.6-fpm' ]; then
cat << EOF
    && apt-get -y install php-http php-http-request \\
# php5-pecl-http
EOF
fi

cat << EOF
	&& curl -sL https://deb.nodesource.com/setup_6.x | bash - \\
	&& apt-get install nodejs \\
	&& npm install -g grunt-cli \\

	## Cleanup
	&& apt-get remove -y binutils \\
	&& apt-get -y autoremove \\
	&& apt-get -y clean \\
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/src/php


# config file
COPY php.ini /usr/local/etc/php/php.ini
COPY php-fpm.conf /usr/local/etc/php/conf.d/php-fpm.ini
COPY www.conf /usr/local/etc/php/conf.d/pool.d/www.ini
COPY docker.conf /usr/local/etc/php-fpm.d/docker.conf
# COPY opcache.conf /usr/local/etc/php-fpm.d/opcache.conf
COPY bashrc /root/.bashrc

RUN echo 'extension=apc.so' >> /usr/local/etc/php/conf.d/docker-php-ext-apcu.ini

# RUN rm /usr/local/etc/php-fpm.d/docker.conf
# RUN rm /usr/local/etc/php-fpm.d/zz-docker.conf

# CA cert
# RUN mkdir -p /etc/pki/tls/certs && mkdir -p /usr/local/var/log/php && chown www-data:www-data /usr/local/var/log/php

# Create user for code
#RUN echo "code:x:1000:code,www-data" >> /etc/group
#RUN adduser --uid 1000 code
#RUN usermod -a -G www-data code

# ESTO ERA LO BUENO
#RUN useradd code -s /bin/bash -u 1000 -U -G www-data
#RUN usermod -a -G code,www-data code

ADD ./start.sh /start.sh
#RUN chmod +x /usr/local/bin/start.sh

CMD [ "/start.sh" ]

# CMD ["php-fpm"]
# CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

EOF
