Docker Images
=============

[![pipeline status](https://gitlab.asmws.com/jrodriguez/docker/badges/master/pipeline.svg)](https://gitlab.asmws.com/jrodriguez/docker/commits/master)

LEMP
----

Execute this command to run a LEMP (Nginx+PHP7+MySQL) server:

```bash
$ docker run --name lemp -d gitlab.asmws.com/jrodriguez/docker/lemp:master
```

SAMBA
-----

Make all docker volumes accesible from windows. *Do not use this in a production environment.*

```bash
docker run --name samba -d -v /var/lib/docker/volumes:/docker_volumes -p 139:139 -p 445:445 -v /var/run/docker.sock:/var/run/docker.sock --net=host gitlab.asmws.com:5000/jrodriguez/docker/samba:master
```

SSH
---

SSH access to all running docker containers. Default user/pass: asmuser / asmws120

```bash
docker run -ti --rm --name ssh -v /var/run/docker.sock:/var/run/docker.sock gitlab.asmws.com:5000/jrodriguez/docker/ssh:master
```

Keys:
* F10 New window
* F11 Go to previous window
* F12 Go to next window
