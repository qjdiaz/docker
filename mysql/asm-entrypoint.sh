#!/bin/bash

if [ ! -z "$MYSQL_CFG_MAX_ALLOWED_PACKET" ];	then echo "max_allowed_packet=$MYSQL_CFG_MAX_ALLOWED_PACKET" >> /etc/mysql/mysql.conf.d/mysqld.cnf ; fi
if [ ! -z "$MYSQL_CFG_QUERY_CACHE_LIMIT" ];		then echo "query_cache_limit=$MYSQL_CFG_QUERY_CACHE_LIMIT" >> /etc/mysql/mysql.conf.d/mysqld.cnf ; fi
if [ ! -z "$MYSQL_CFG_QUERY_CACHE_SIZE" ];		then echo "query_cache_size=$MYSQL_CFG_QUERY_CACHE_SIZE" >> /etc/mysql/mysql.conf.d/mysqld.cnf ; fi
if [ ! -z "$MYSQL_CFG_QUERY_CACHE_TYPE" ];		then echo "query_cache_type=$MYSQL_CFG_QUERY_CACHE_TYPE" >> /etc/mysql/mysql.conf.d/mysqld.cnf ; fi

source /usr/local/bin/docker-entrypoint.sh