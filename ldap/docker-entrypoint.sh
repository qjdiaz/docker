#!/bin/bash

if [ ! -z "$LDAP_SERVERS" ];	then 
	sed -i 's|ldap:\/\/ad.example.com:389\/|'"$LDAP_SERVERS"'|' /etc/saslauthd.conf 
	sed -i 's|ad.example.com|'"$LDAP_SERVERS_DOMAIN"'|' /var/ldap-scripts/cron.sh
fi
if [ ! -z "$LDAP_SEARCH_BASE" ];	then 
	sed -i 's/CN=DomainUsers,DC=example,DC=com/'"$LDAP_SEARCH_BASE"'/' /etc/saslauthd.conf 
fi
if [ ! -z "$LDAP_TIMEOUT" ];	then 
	sed -i 's/ldap_timeout: 10/ldap_timeout: '"$LDAP_TIMEOUT"'/' /etc/saslauthd.conf 
fi
if [ ! -z "$LDAP_FILTER" ];	then 
	sed -i 's/(sAMAccountName=%U)/'"$LDAP_FILTER"'/' /etc/saslauthd.conf 
fi
if [ ! -z "$LDAP_BIND_DN" ];	then 
	sed -i 's/CN=Administrator,CN=Users,DC=example,DC=com/'"$LDAP_BIND_DN"'/' /etc/saslauthd.conf 
	sed -i 's/CN=Administrator,CN=Users,DC=example,DC=com/'"$LDAP_BIND_DN"'/' /var/ldap-scripts/cron.sh
fi
if [ ! -z "$LDAP_PASSWORD" ];	then 
	sed -i 's/ADpassword/'"$LDAP_PASSWORD"'/' /etc/saslauthd.conf
	sed -i 's/ADpassword/'"$LDAP_PASSWORD"'/' /var/ldap-scripts/cron.sh
fi

if [ ! -z "$LDAP_DN_BASE" ];	then 
	sed -i 's/suffix\s\+"dc=my-domain,dc=com"/suffix          \"'"$LDAP_DN_BASE"'\"/' /usr/local/etc/openldap/slapd.conf
fi
if [ ! -z "$LDAP_ROOT_DN" ];	then 
	sed -i 's/cn=Manager,dc=my-domain,dc=com/'"$LDAP_ROOT_DN"'/' /usr/local/etc/openldap/slapd.conf 
	sed -i 's/cn=Manager,dc=my-domain,dc=com/'"$LDAP_ROOT_DN"'/' /var/ldap-scripts/importAsmUsers.sh 
fi
if [ ! -z "$LDAP_DATABASE" ];	then 
	sed -i 's/database\s\+mdb/database        '"$LDAP_DATABASE"'/' /usr/local/etc/openldap/slapd.conf
fi
if [ ! -z "$LDAP_ROOT_PW" ];	then 
	sed -i 's/rootpw\s\+secret/rootpw          '"$LDAP_ROOT_PW"'/' /usr/local/etc/openldap/slapd.conf
	sed -i 's/secret/'"$LDAP_ROOT_PW"'/' /var/ldap-scripts/importAsmUsers.sh 
fi

if [ ! -z "$LDAP_DN_BASE" ];	then 
	sed -i 's/dn: dc=my-domain,dc=com/dn: '"$LDAP_DN_BASE"'/' /usr/local/etc/openldap/create.ldif
	sed -i 's/dc=my-domain,dc=com/'"$LDAP_DN_BASE"'/' /usr/local/etc/openldap/create.ldif 
	slapadd -l /usr/local/etc/openldap/create.ldif
	sed -i 's/dc=my-domain,dc=com/'"$LDAP_DN_BASE"'/' /var/ldap-scripts/importAsmUsers.sh
fi
if [ ! -z "$LDAP_ORGANIZATION" ];	then 
	sed -i 's/o: Example/o: '"$LDAP_ORGANIZATION"'/' /usr/local/etc/openldap/create.ldif
fi

if [ "$RECREATE" == "true"];	then 
	slapadd -l /usr/local/etc/openldap/create.ldif
fi



service saslauthd start
/usr/local/libexec/slapd -d 1024
