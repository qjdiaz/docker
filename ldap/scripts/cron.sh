#!/bin/bash

ldapsearch -x -h "LDAP_SERVERS_DOMAIN" -D "LDAP_BIND_DN" -w LDAP_PASSWORD -p 389 -b "" -s sub '(&(objectClass=*)(&(objectclass=inetOrgPerson)))' cn mail userpassword dn displayname uid sn givenname > "files/asm.ldif"

cat "files/asm.ldif" | ./importAsmUsers.sh cn mail userpassword dn displayname uid sn givenname