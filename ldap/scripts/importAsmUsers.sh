#!/bin/bash
#
# Converts LDIF data to CSV.
# Doesn't handle comments very well. Use -LLL with ldapsearch to remove them.
#
# 2010-03-07
# dsimmons@squiz.co.uk
#

# Show usage if we don't have the right params
FILE=files/a.out.tmp
USERFILE=files/user.in.tmp

function searchldap {
    # echo "Parameter #1 es $1"
  ldapsearch -x -D 'LDAP_ROOT_DN' -w LDAP_ROOT_PW -b "LDAP_DN_BASE" -s sub "(&(&(objectClass=*))(&(cn=${1})))" > $FILE
  readarray -t lines < "$FILE"
  userdata[numEntries]="0"
  for line in "${lines[@]}"; do
    # echo "'${line:0:12}'"
    if [  "${line:0:12}" == "# numEntries"  ]; then
      arrIN=(${line//:/ })
        key="${arrIN[0]}"
        value="${arrIN[1]}"
        userdata[numEntries]=$value
        # echo 'ddd'
      fi
      [ "${line:0:1}" == "#" ] && continue;
      [ "${line:0:1}" == "" ] && continue;
      arrIN=(${line//:/ })
      key="${arrIN[0]}"
      value="${arrIN[1]}"
      userdata[$key]=$value
  done
  # exit
}

function getMaxUid ()
{
  n=0
  for i in $(ldapsearch -x -w LDAP_ROOT_PW -D "LDAP_ROOT_DN"  -b 'LDAP_DN_BASE' "(uidNumber=*)" uidNumber -S uidNumber | grep uidNumber | tail -n1 );
  do 
    ldaparry[$n]=$i
    let n+=1
  done

    if [ "${ldaparry[0]}" == "uidNumber:" ]; then
    echo $((${ldaparry[1]}+1))
    return 0
    else
      echo 1000
    return 0
    fi
}


function createUserldap {
  ldapadd -x -w LDAP_ROOT_PW -D "LDAP_ROOT_DN" -f $USERFILE
}

function updateUserldap {
  ldapmodify -x -w LDAP_ROOT_PW -D "LDAP_ROOT_DN" -f $USERFILE
}


if [ "$1" == "" ]; then
  echo ""
  echo "Usage: cat ldif.txt | $0 <attributes> [...]"
  echo "Where <attributes> contains a list of space-separated attributes to include in the CSV. LDIF data is read from stdin."
  echo ""
  exit 99
fi

ATTRS="$*"

c=0
while read line; do

  # Skip LDIF comments
  [ "${line:0:1}" == "#" ] && continue;

  # If this line is blank then it's the end of this record, and the beginning
  # of a new one.
  #
  if [ "$line" == "" ]; then

    output=""

    declare -A arr
    declare -A userdata
    # Output the CSV record
    for i in $ATTRS; do

      eval data=\$RECORD_${c}_${i}
      output2=${output}\"${data}\",
      # echo $i
      if [ "$output2" != '"",' ]; then
        output=${output}\"${data}\",
        unset RECORD_${c}_${i}
        # ${arr[$i]}=$data
        arr[$i]=$data
      fi

    done
    if [ "$output" != "" ]; then
      # echo ${arr[uid]}
      # echo ${arr[cn]}
      # echo ${arr[mail]}
      # echo ${arr[userpassword]}
      # echo ${arr[dn]}
      # echo ${arr[displayname]}
      # echo ${arr[sn]}
      # echo ${arr[givenname]}
      # declare -A userdata
      # if [ "${arr[uid]}" == "dpalacios" ]; then
        searchldap "${arr[uid]}"
        # echo "${userdata[numEntries]}"
        if [ "${userdata[numEntries]}" == "0" ]; then
          # echo ${arr[givenname]}
          arr[dn]="cn=${arr[uid]},ou=users,LDAP_DN_BASE"
          arr[cn]="${arr[uid]}"
          arr[cn]="${arr[uid]}"
          arr[uid]="${arr[uid]}"
          arr[objectclass]="inetOrgPerson"
          arr[gidnumber]="500"
          arr[homeDirectory]="/home/users/${arr[uid]}"
          arr[uidnumber]=`getMaxUid`
          arr[userpassword]="{SASL}${arr[uid]}"
          echo ${arr[uid]}
          printf "%s\n" "# CREATE USER" > $USERFILE
          for key in "${!arr[@]}"
          do
             printf "%s\n" "${key}: ${arr[${key}]}" >> $USERFILE
             if [ "${key}" == "objectclass" ]; then
                printf "%s\n" "${key}: posixAccount" >> $USERFILE
                printf "%s\n" "${key}: top" >> $USERFILE
             fi
          done
          # CREAR USUARIO
          # echo ${arr[uid]}
          if [ "${arr[givenname]}" != "" ]; then
            createUserldap
          fi
        fi
        # exit
      # fi
    fi
    # Remove trailing ',' and echo the output
    output=${output%,}
    # echo $output
    # exit

    # Increase the counter
    c=$(($c+1))
  fi

  # Separate attribute name/value at the semicolon (LDIF format)
  attr=${line%%:*}
  value=${line#*: }

  # Save all the attributes in variables for now (ie. buffer), because the data
  # isn't necessarily in a set order.
  #
  for i in $ATTRS; do
    if [ "$attr" == "$i" ]; then
      eval RECORD_${c}_${attr}=\"$value\"
    fi
  done

done