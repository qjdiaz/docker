# Example ldap proxy

## docker-compose.yml example
```
version: '2'

services:
    ldap:
        image:gitlab.asmws.com/jrodriguez/docker/ldap:master
        environment:
            LDAP_SERVERS: "ldap://ldap.test.com:389/" 
            LDAP_SEARCH_BASE: "O=ORGANIZATION" 
            LDAP_FILTER: "(uid=%U)"
            LDAP_BIND_DN: "user" 
            LDAP_PASSWORD: "password" 
            LDAP_DN_BASE: "dc=dmldap,dc=local" 
            LDAP_ORGANIZATION: "users" 
            LDAP_ROOT_DN: "cn=admin,dc=dmldap,dc=local" 
            LDAP_ROOT_PW: "password"
            RECREATE: "true"
        ports:
          - "389:389"
          - "639:639"
```
