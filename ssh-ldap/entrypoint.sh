#!/bin/bash

echo -e "BASE ${LDAP_BASE}\nURI ${LDAP_SERVER}" > /etc/nslcd.conf
echo -e "binddn $ADMIN_DN\nbindpw $ADMIN_DN_PASS" >> /etc/nslcd.conf

for item in passwd shadow group; do
    sed -i "s/^${item}:.*/${item}: ldap compat /g" /etc/nsswitch.conf
done

# clear motd 
sed -i "s/^PrintLastLog yes/PrintLastLog no/g" /etc/ssh/sshd_config
sed -i "s/^PermitRootLogin without-password/PermitRootLogin no/g" /etc/ssh/sshd_config
# sed -i "s/PasswordAuthentication yes/d" /etc/ssh/sshd_config

# echo -e "\nForceCommand /usr/local/bin/record_ssh.sh" >> /etc/ssh/sshd_config

echo "" > /etc/motd

if [[ $(grep -c "Cmnd_Alias DOCKER=/usr/bin/docker" /etc/sudoers ) -eq 0 ]]; then
  echo "Cmnd_Alias DOCKER=/usr/bin/docker" >> /etc/sudoers
fi
if [[ $(grep -c "ALL ALL=NOPASSWD: DOCKER" /etc/sudoers ) -eq 0 ]]; then
  echo "ALL ALL=NOPASSWD: DOCKER" >> /etc/sudoers
fi

groupadd -f -g 1000 data

sed -i "s/ADMIN_DN_PASS/$ADMIN_DN_PASS/g" /usr/bin/dshell
sed -i "s/ADMIN_DN/$ADMIN_DN/g" /usr/bin/dshell
sed -i "s/LDAP_BASE/$LDAP_BASE/g" /usr/bin/dshell
sed -i "s/LDAP_SERVER_PLAIN/$LDAP_SERVER_PLAIN/g" /usr/bin/dshell

# sed -i "s/ADMIN_DN /$ADMIN_DN/g" /etc/profile.d/userlist.sh
# sed -i "s/ADMIN_DN_PASS/$ADMIN_DN_PASS/g" /etc/profile.d/userlist.sh
# sed -i "s/LDAP_BASE/$LDAP_BASE/g" /etc/profile.d/userlist.sh

# if [[ $(grep -c "ForceCommand /usr/bin/dshell" /etc/ssh/sshd_config ) -eq 0 ]]; then   
#    echo "ForceCommand /usr/bin/dshell" >> /etc/ssh/sshd_config
# fi

sed -i "s/^Subsystem sftp /usr/lib/openssh/sftp-server/Subsystem sftp internal-sftp/g" /etc/ssh/sshd_config
# if [[ $(grep -c "ChrootDirectory %h" /etc/ssh/sshd_config ) -eq 0 ]]; then   
#    echo "ChrootDirectory %h" >> /etc/ssh/sshd_config
# fi

if [[ $(grep -c "ForceCommand /usr/bin/tmux2" /etc/ssh/sshd_config ) -eq 0 ]]; then   
   echo "ForceCommand /usr/bin/tmux2" >> /etc/ssh/sshd_config
fi
if [[ $(grep -c "session required pam_mkhomedir.so" /etc/pam.d/common-session ) -eq 0 ]]; then   
   echo "session required pam_mkhomedir.so" >> /etc/pam.d/common-session
fi


# ssh public key access config
# if [[ $(grep -c "AuthorizedKeysCommand " /etc/ssh/sshd_config ) -eq 0 ]]; then   
#   echo "AuthorizedKeysCommand /ldap_auth/ldap_auth.sh" >> /etc/ssh/sshd_config
# fi
# if [[ $(grep -c "AuthorizedKeysCommandUser " /etc/ssh/sshd_config ) -eq 0 ]]; then
#   echo "AuthorizedKeysCommandUser nobody" >> /etc/ssh/sshd_config
# fi
# if [[ $(grep -c "AuthorizedKeysFile /dev/null" /etc/ssh/sshd_config ) -eq 0 ]]; then
#   echo "AuthorizedKeysFile /dev/null" >> /etc/ssh/sshd_config
# fi

# if [[ $(grep -c "PasswordAuthentication no" /etc/ssh/sshd_config ) -eq 0 ]]; then
#   echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
# fi

# echo "BASTION_SERVER_IP $BASTION_SERVER_IP" > /BASTION_SERVER
# echo "BASTION_SERVER_PORT $BASTION_SERVER_PORT" >> /BASTION_SERVER


if /usr/sbin/nslcd ; then
  echo "run nslcd"
fi
if /usr/sbin/rsyslogd; then
  echo "run rsyslogd"
fi
if /usr/sbin/sshd -D ; then
  echo "run sshd"
fi