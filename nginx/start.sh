#!/bin/bash

TEMPLATEFILE="vhost.conf.tmpl"

if [[ ! -z "$FRAMEWORK" ]]; then
	TEMPLATEFILE="vhost_${FRAMEWORK}.conf.tmpl"
fi

INFILE="/etc/nginx/conf.d/${TEMPLATEFILE}"
OUTFILE="/etc/nginx/conf.d/vhost.conf"

cp ${INFILE} ${OUTFILE}

if [[ ! -z "${ACCESS_LOG}" ]]; then
    sed -i "s#{{ACCESS_LOG}}#${ACCESS_LOG}#" /etc/nginx/nginx.conf
else
	sed -i "s#{{ACCESS_LOG}}#/var/log/nginx/access.log main#" /etc/nginx/nginx.conf
fi

if [[ ! -z "${ERROR_LOG}" ]]; then
    sed -i "s#{{ERROR_LOG}}#${ERROR_LOG}#" /etc/nginx/nginx.conf
else
	sed -i "s#{{ERROR_LOG}}#/var/log/nginx/error.log warn#" /etc/nginx/nginx.conf
fi


# Si se especifica el backend, configurarlo
if [[ ! -z "${FASTCGI_BACKEND}" ]]; then
	sed -i "s/{{FASTCGI_BACKEND}}/${FASTCGI_BACKEND}/" /etc/nginx/nginx.conf
else
	sed -i "s/{{FASTCGI_BACKEND}}/php/" /etc/nginx/nginx.conf
fi

# Si se especifica el backend (para SSL) configurarlo
if [[ ! -z "${SSL_BACKEND}" ]]; then
	sed -i "s/{{SSL_BACKEND}}/${SSL_BACKEND}/" ${OUTFILE}
else
	sed -i "s/{{SSL_BACKEND}}/varnish/" ${OUTFILE}
fi



if [[ -z "${FASTCGI_READ_TIMEOUT}" ]]; then
	FASTCGI_READ_TIMEOUT=60
fi

sed -i "s/{{FASTCGI_READ_TIMEOUT}}/${FASTCGI_READ_TIMEOUT}/" /etc/nginx/nginx.conf


# Using environment variables to set nginx configuration
#[ -z "${WEB_DOMAIN}" ] && echo "\$WEB_DOMAIN is not set" || sed -i "s/WEB_DOMAIN/${WEB_DOMAIN}/" /etc/nginx/conf.d/vhost.conf
#[ -z "${WEB_DOMAIN}" ] && echo "\$WEB_DOMAIN is not set" || sed "s/WEB_DOMAIN/${WEB_DOMAIN}/" ${OUTFILE} > ${OUTFILE}

[ -z "${WEB_DOMAIN}" ] && echo "\$WEB_DOMAIN is not set" || sed -i "s/{{WEB_DOMAIN}}/${WEB_DOMAIN}/" ${OUTFILE}

# Definicion de tiendas para magento 1
if [[ ! -z "${MAGENTO_SITE_1}" ]]; then
        IFS=' ' read -r -a array <<< "$MAGENTO_SITE_1"
        sed -i "s/{{MAGE_CODE_1}}/${array[0]} ${array[1]};/" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_1}}/${array[0]} ${array[2]};/" ${OUTFILE}
else
        sed -i "s/{{MAGE_CODE_1}}//" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_1}}//" ${OUTFILE}
fi

if [[ ! -z "${MAGENTO_SITE_2}" ]]; then
        IFS=' ' read -r -a array <<< "$MAGENTO_SITE_2"
        sed -i "s/{{MAGE_CODE_2}}/${array[0]} ${array[1]};/" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_2}}/${array[0]} ${array[2]};/" ${OUTFILE}
else
        sed -i "s/{{MAGE_CODE_2}}//" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_2}}//" ${OUTFILE}
fi

if [[ ! -z "${MAGENTO_SITE_3}" ]]; then
        IFS=' ' read -r -a array <<< "$MAGENTO_SITE_3"
        sed -i "s/{{MAGE_CODE_3}}/${array[0]} ${array[1]};/" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_3}}/${array[0]} ${array[2]};/" ${OUTFILE}
else
        sed -i "s/{{MAGE_CODE_3}}//" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_3}}//" ${OUTFILE}
fi

if [[ ! -z "${MAGENTO_SITE_4}" ]]; then
        IFS=' ' read -r -a array <<< "$MAGENTO_SITE_4"
        sed -i "s/{{MAGE_CODE_4}}/${array[0]} ${array[1]};/" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_4}}/${array[0]} ${array[2]};/" ${OUTFILE}
else
        sed -i "s/{{MAGE_CODE_4}}//" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_4}}//" ${OUTFILE}
fi

if [[ ! -z "${MAGENTO_SITE_5}" ]]; then
        IFS=' ' read -r -a array <<< "$MAGENTO_SITE_5"
        sed -i "s/{{MAGE_CODE_5}}/${array[0]} ${array[1]};/" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_5}}/${array[0]} ${array[2]};/" ${OUTFILE}
else
        sed -i "s/{{MAGE_CODE_5}}//" ${OUTFILE}
        sed -i "s/{{MAGE_TYPE_5}}//" ${OUTFILE}
fi

# Modo para magento 2
MAGE_MODE="default";
if [[ ! -z "$MAGENTO_MODE" ]]; then
	MAGE_MODE="${MAGENTO_MODE}"
fi
sed -i "s/{{MAGENTO_MODE}}/${MAGE_MODE};/" ${OUTFILE}


# Certificados SSL
if [[ -f /var/www/html/ssl/${WEB_DOMAIN}.key ]]; then
    sed -i "s/{{SSL_CONFIG}}/listen 443 ssl;\nssl_certificate \/var\/www\/html\/ssl\/${WEB_DOMAIN}.crt;\nssl_certificate_key \/var\/www\/html\/ssl\/${WEB_DOMAIN}.key;/" ${OUTFILE}
else
	sed -i "s/{{SSL_CONFIG}}//" ${OUTFILE}
fi



# Start nginx
#/usr/sbin/nginx -D FOREGROUND
nginx

cat ${OUTFILE}
