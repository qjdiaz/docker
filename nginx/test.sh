#!/bin/bash

MAGENTO_SITE_1="test.es test_es website"

if [[ ! -z "${MAGENTO_SITE_1}" ]]; then
	IFS=' ' read -r -a array <<< "$MAGENTO_SITE_1"
	echo "${array[0]} ${array[2]}"
fi
