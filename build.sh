#!/bin/bash

#REPO=gitlab.asmws.com:5000/jrodriguez/docker
REPO=gitlab.com:5000/qjdiaz/docker

# PHP-FPM
( cd php-fpm; ./gen.sh )
#docker build -t ${REPO}/php-fpm:5.6 -f ./php-fpm/Dockerfile-5.6-fpm ./php-fpm
#docker build -t ${REPO}/php-fpm:7.0 -f ./php-fpm/Dockerfile-7.0-fpm ./php-fpm
docker build -t ${REPO}/php-fpm:7.1 -f ./php-fpm/Dockerfile-7.1-fpm ./php-fpm

#docker push ${REPO}/php-fpm:5.6
#docker push ${REPO}/php-fpm:7.0
docker push ${REPO}/php-fpm:7.1

# NGINX
docker build -t ${REPO}/nginx ./nginx

docker push ${REPO}/nginx

# MYSQL
docker build -t ${REPO}/mysql ./mysql

#docker push ${REPO}/mysql